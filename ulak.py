#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import logging

from yaml import load, FullLoader
from bunch import bunchify
from collections import OrderedDict

from lib.lib_zabbix import Zabbix
from lib.lib_pdf import PDFReport
from lib.lib_email import Mail
from lib.lib_components import calculate_time_difference, clock_works


KNOWN_PERIODS = {
    "daily": lambda: {"days": 1},
    "weakly": lambda: {"weeks": 1},
    "monthly": lambda: {"weeks": 4}
}


def load_config():
    return load(open("config.yaml", "r").read(), Loader=FullLoader)


def parse_args():
    parser = argparse.ArgumentParser(description="Ulak Zabbix PDF Report Creator")
    parser.add_argument("--report-kind", action="store", dest="report_kind", help="Kind for report.", default="checklist", choices=["resource_tracking", "event", "checklist"])
    return parser.parse_args()


def email_to_report(report_path):
    # Send to report via e-mail
    email.send_mail(
        subject=f"[Ulak Zabbix Reporter] {config.report.period.capitalize()} {args.report_kind} report for infrastructure.",
        body="Your report is attached.",
        recipients={"to": config.recipients.to, "cc": config.recipients.cc, "bcc": config.recipients.bcc},
        html=True,
        attach=report_path
    )


def event(host_objects):
    t_since, t_till = clock_works(_type="unix_timestamp", convert=True, convert_date_object=calculate_time_difference(KNOWN_PERIODS[config.report.period]())), clock_works(_type="unix_timestamp")
    event_objects = z_api.get_events_belongs_to_host(host_objects=host_objects, t_since=t_since, t_till=t_till)
    report_header = OrderedDict({
        "Event": None,
        "State": None,
        "Timestamp": None,
        "Acknowledge": None,
        "Acknowledged By": None,
        "Acknowledge Timestamp": None
    })

    for host_name, event_object in event_objects.items():
        reporter.make_report(
            table_header=f"{host_name} Events",
            report_headers=report_header.keys(),
            report_data=event_object
        )

    # E-mail process
    email_to_report(reporter.save_report())


def resource_tracking(host_objects):
    t_since, t_till = clock_works(_type="unix_timestamp", convert=True, convert_date_object=calculate_time_difference(KNOWN_PERIODS[config.report.period]())), clock_works(_type="unix_timestamp")
    item_objects = z_api.get_resource_usage_differentiation(host_objects=host_objects, item_names=config.report.resourceItems, t_till=t_till, t_since=t_since)
    report_header = OrderedDict({"Host": None})
    for item_name in config.report.resourceItems:
        report_header[item_name] = None

    reporter.make_report(
        table_header="Resource Usage Statistics for Hosts (Average)",
        report_headers=report_header.keys(),
        report_data=item_objects,
        is_dict=True
    )

    # E-mail process
    email_to_report(reporter.save_report())


def checklist(host_objects):
    item_objects = z_api.get_item_lastvalue_list_belongs_to_host(host_objects=host_objects, item_names=config.report.resourceItems)
    report_header = OrderedDict({"Host": None})
    for item_name in config.report.resourceItems:
        report_header[item_name] = None

    reporter.make_report(
        table_header="Checklist for Hosts",
        report_headers=report_header.keys(),
        report_data=item_objects,
        is_dict=True
    )

    # E-mail process
    email_to_report(reporter.save_report())


def main():
    host_objects = None
    if config.report.get("hosts"):
        host_objects = z_api.get_host_objects(config.report.hosts)

    # If host group defined in configuration, merge with this
    if config.report.get("hostGroups"):
        host_objects = z_api.get_host_objects_belongs_to_groups(config.report.hostGroups)

    # Call report kind function
    globals()[args.report_kind](host_objects)


if __name__ == "__main__":
    # General arguments
    args = parse_args()

    # Application configs
    config = bunchify(
        load_config()
    )

    # Logging configs
    logging.basicConfig(
        level=config.logging.level,
        filename=config.logging.fileName
    )

    # Zabbix API object
    z_api = Zabbix(
        url=config.zabbix.url,
        username=config.zabbix.username,
        password=config.zabbix.password
    )

    # PDF creator object
    reporter = PDFReport(
        config=config
    )

    # Email object
    email = Mail(
        from_address=config.sender.fromAddress,
        from_mail=config.sender.mailAddress,
        from_mail_password=config.sender.password,
        mail_server=config.sender.mailServer,
        mail_server_port=config.sender.mailServerPort
    )

    # Jum to the action
    main()
