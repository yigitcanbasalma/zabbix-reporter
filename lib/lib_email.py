#!/usr/bin/python
# -*- coding: utf-8 -*-

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from os import environ

import smtplib


class Mail(object):
    def __init__(self, from_address, from_mail, from_mail_password, mail_server, mail_server_port):
        self.email = from_mail.value if not from_mail.get("fromEnv") else environ[from_mail.fromEnv.value]
        self.password = from_mail_password.value if not from_mail_password.get("fromEnv") else environ[from_mail_password.fromEnv.value]
        self.server = mail_server.value if not mail_server.get("fromEnv") else environ[mail_server.fromEnv.value]
        self.port = int(mail_server_port.value if not mail_server_port.get("fromEnv") else environ[mail_server_port.fromEnv.value])
        self.session = None
        self.mail_from = from_address.value if not from_address.get("fromEnv") else environ[from_address.fromEnv.value]

    def start_session(self):
        session = smtplib.SMTP(self.server, self.port)
        session.ehlo()
        if self.port == 587:
            session.starttls()
            session.ehlo()
            session.login(self.email, self.password)
        self.session = session

    def send_mail(self, subject, body, recipients, html=False, attach=None):
        self.start_session()
        recipients_addr = []
        for k, v in recipients.items():
            if k in ["to", "cc", "bcc"]:
                recipients_addr.extend(v)
        if not html:
            headers = [
                "From: " + self.mail_from,
                "Subject: " + subject,
                "To: " + ";".join(recipients["to"])]
            if "cc" in recipients:
                headers.append(
                    "Cc: " + ";".join(recipients["cc"])
                )
            if "bcc" in recipients:
                headers.append(
                    "Bcc: " + ";".join(recipients["bcc"])
                )
            headers = "\r\n".join(headers)
            self.session.sendmail(
                self.email,
                recipients_addr,
                headers + "\r\n\r\n" + body
            )
        else:
            msg = MIMEMultipart('alternative')
            msg['Subject'] = subject
            msg['From'] = self.mail_from
            msg['To'] = ";".join(recipients["to"])
            if "cc" in recipients:
                msg["Cc"] = ";".join(recipients["cc"])
            if "bcc" in recipients:
                msg["Bcc"] = ";".join(recipients["cc"])
            part2 = MIMEText(body, "html", "utf-8")
            msg.attach(part2)
            if attach is not None:
                filename = attach
                attachment = open(filename, "rb")
                part = MIMEBase('application', 'octet-stream')
                part.set_payload(attachment.read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', "attachment; filename= %s" % filename.split("/")[-1])
                msg.attach(part)
            self.session.sendmail(
                self.email,
                recipients_addr,
                msg.as_string()
            )
        return True
