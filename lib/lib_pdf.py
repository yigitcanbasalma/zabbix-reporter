#!/usr/bin/python
# -*- coding: utf-8 -*-

from jinja2 import Template
from pdfkit import from_string

import os

from lib.lib_components import get_uuid


class PDFReport(object):

    def __init__(self, config):
        self.config = config
        self.report_file = f"{self.config.report.period}_{get_uuid()}.pdf"
        self.path = os.path.join(self.config.general.reportsPath, self.report_file)
        self.template = {
            "report": Template(open("templates/report.html.j2", "r").read()),
            "table": Template(open("templates/table.html.j2", "r").read())
        }
        self.report_data = list()

    def make_report(self, table_header, report_headers, report_data, is_dict=False):
        if len(report_data) > 0:
            self.report_data.append(
                self.template["table"].render(table_header=table_header, report_headers=report_headers, report_data=report_data, is_dict=is_dict)
            )

    def save_report(self):
        from_string(self.template["report"].render(table="\n".join(self.report_data)), self.path)
        return self.path
