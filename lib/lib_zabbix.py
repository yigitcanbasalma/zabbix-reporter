#!/usr/bin/python
# -*- coding: utf-8 -*-

from pyzabbix import ZabbixAPI
from os import environ
from logging import getLogger

from lib.lib_components import unit_format, calculate_avg_and_differentiation, clock_works


class Zabbix(ZabbixAPI):
    KNOWN_EVENT_STATUSES = {
        0: ("Solved", "green"),
        1: ("Active", "red")
    }

    def __init__(self, url, username, password):
        super().__init__(url.value if not url.get("fromEnv") else environ[url.fromEnv.value], verify=False)
        self.login(
            user=username.value if not username.get("fromEnv") else environ[username.fromEnv.value],
            password=password.value if not password.get("fromEnv") else environ[password.fromEnv.value]
        )
        self.logger = getLogger("zabbix-api-handler")

    def _get_host_group(self, name):
        argument_dict = {
            "filter": {
                "name": name
            }
        }
        return self.do_request(
            "hostgroup.get",
            params=argument_dict
        )["result"]

    def _get_host(self, name=None, group_id=None):
        argument_dict = {
            "output": "extend",
            "filter": {
                "name": name
            }
        }
        if group_id:
            del argument_dict["filter"]
            argument_dict["groupids"] = [group_id]
        return self.do_request(
            "host.get",
            params=argument_dict
        )["result"]

    def _get_item(self, item_name, host_id):
        argument_dict = {
            "output": "extend",
            "hostids": [host_id],
            "filter": {
                "name": item_name
            }
        }
        return self.do_request(
            "item.get",
            params=argument_dict
        )["result"]

    def _get_history(self, item_id, item_type_id, t_since, t_till, limit=None):
        argument_dict = {
            "output": "extend",
            "history": item_type_id,
            "itemids": [item_id],
            "time_from": t_since,
            "time_till": t_till,
            "sortfield": "clock",
            "sortorder": "DESC"
        }
        if limit:
            argument_dict["limit"] = limit
        return self.do_request(
            "history.get",
            params=argument_dict
        )["result"]

    def _get_event(self, host_id, t_since, t_till):
        argument_dict = {
            "output": "extend",
            "select_acknowledges": "extend",
            "hostids": [host_id],
            "time_from": t_since,
            "time_till": t_till,
            "sortfield": ["clock", "eventid"],
            "sortorder": "DESC"
        }
        return self.do_request(
            "event.get",
            params=argument_dict
        )["result"]

    def _get_host_group_objects(self, host_group_names):
        host_group_objects = list()
        for host_group_name in host_group_names:
            host_group_objects.extend(
                self._get_host_group(name=host_group_name)
            )
        return host_group_objects

    def get_host_objects(self, host_names):
        host_objects = list()
        for host_name in host_names:
            host_objects.extend(
                self._get_host(name=host_name)
            )
        return host_objects

    def get_host_objects_belongs_to_groups(self, host_group_names):
        host_objects = list()
        for host_group_object in self._get_host_group_objects(host_group_names=host_group_names):
            host_objects.extend(
                self._get_host(group_id=host_group_object["groupid"])
            )
        return host_objects

    def get_item_lastvalue_list_belongs_to_host(self, host_objects, item_names):
        item_objects = dict()
        for host_object in host_objects:
            item_objects[host_object["name"]] = dict(Host=(host_object["name"], "black"))
            for item_name in item_names:
                item_object = self._get_item(item_name, host_id=host_object["hostid"])
                if len(item_object) == 0:
                    item_objects[host_object["name"]][item_name] = (unit_format("None", "None"), "black")
                    continue
                item_objects[host_object["name"]][item_name] = (unit_format(item_object[0]["lastvalue"], item_object[0]["units"]), "black")
        return item_objects

    def get_resource_usage_differentiation(self, host_objects, item_names, t_since, t_till):
        item_objects = dict()
        for host_object in host_objects:
            item_objects[host_object["name"]] = dict(Host=(host_object["name"], "black"))
            for item_name in item_names:
                item_object = self._get_item(item_name, host_id=host_object["hostid"])
                if len(item_object) == 0:
                    item_objects[host_object["name"]][item_name] = (unit_format("None", "None"), "black")
                    continue
                history_objects = self._get_history(item_object[0]["itemid"], item_object[0]["value_type"], t_since, t_till)
                avg = calculate_avg_and_differentiation(history_objects, int(item_object[0]["value_type"]))
                item_objects[host_object["name"]][item_name] = (unit_format(avg, item_object[0]["units"]), "black")
        return item_objects

    def get_events_belongs_to_host(self, host_objects, t_since, t_till):
        item_objects = dict()
        for host_object in host_objects:
            item_objects[host_object["name"]] = list()
            for event in self._get_event(host_object["hostid"], t_since, t_till):
                item_objects[host_object["name"]].append({
                    "Event": (event["name"], "black"),
                    "State": (self.KNOWN_EVENT_STATUSES[int(event["value"])][0], self.KNOWN_EVENT_STATUSES[int(event["value"])][1]),
                    "Timestamp": (clock_works(_type="standard", convert=True, convert_date_object=event["clock"], from_timestamp=True), "black"),
                    "Acknowledge": ("No", "black") if not int(event["acknowledged"]) else ("Yes", "black"),
                    "Acknowledged By": ("None", "black") if not int(event["acknowledged"]) else (event["acknowledges"][0]["name"], "black"),
                    "Acknowledge Timestamp": ("None", "black") if not int(event["acknowledged"]) else (clock_works(_type="standard", convert=True, convert_date_object=event["acknowledges"][0]["clock"], from_timestamp=True), "black")
                })
        return item_objects
