#!/usr/bin/python
# -*- coding: utf-8 -*-

import uuid

from datetime import datetime, timedelta
from statistics import mean


def get_uuid():
    return str(uuid.uuid4()).lower().split("-")[-1]


def clock_works(_type, convert=None, convert_date_object=None, from_timestamp=False):
    known_types = {
        "standard": "%d/%m/%Y %H:%M:%S",
        "unix_timestamp": "%s"
    }
    if convert:
        if from_timestamp:
            return datetime.fromtimestamp(int(convert_date_object)).strftime(known_types[_type])
        return convert_date_object.strftime(known_types[_type])
    return datetime.now().strftime(known_types[_type])


def calculate_time_difference(delta_object):
    return datetime.now() - timedelta(**delta_object)


def unit_format(value, unit):
    known_units = {
        "b": lambda i: f"{int(i) / 1024 / 1024 / 1024:.2f} GB",
        "kb": lambda i: f"{int(i) / 1024 / 1024:.2f} GB",
        "mb": lambda i: f"{int(i) / 1024:.2f} GB",
        "gb": lambda i: f"{int(i)} GB",
        "tb": lambda i: f"{int(i)} TB",
        "uptime": lambda i: f"{int(i) / 60 / 60 / 24:.1f} Days",
        "%": lambda i: f"{i}%"
    }
    if unit.lower() in known_units:
        return known_units[unit.lower()](value)
    return f"{value}"


def calculate_avg_and_differentiation(history_objects, value_type):
    known_types = {
        0: lambda i: float(i),
        3: lambda i: int(i)
    }
    if value_type not in known_types:
        return 0
    return round(mean([known_types[value_type](i["value"]) for i in history_objects]), 2)
