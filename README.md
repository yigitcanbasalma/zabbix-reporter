## Ulak
Zabbix API hizmetlerini kullanarak, PDF rapor tabloları oluşturup, bunları mail olarak atmayı amaçlar.

## Kullanım Şekli
##### 1-) Crontab
Script için konfigürasyon dosyası hazırlandıktan sonra, aşağıdaki gibi crontab tanımı yapılabilir.
```bash
0 9 * * * ./ulak.py [--report-kind (checklist, utilization, event)]
```

##### 2-) Ansible AWX (Tower)
Yine yukarıdaki gibi crontab ile script çalıştırılabilir. Fakat bu seçenekte, sunucunun crontabı yerine Ansible üzerinde cron tanımı yapılır.

## Konfigürasyon
Uygulama içerisinde gelen dosyanın adını aşağıdaki gibi değiştirin.
```bash
mv config.example.yaml config.yaml
```

PDF rapor oluşturma için gerekli kütüphaneyi, örnek olarak aşağıki gibi yükleyebilirsiniz.
```bash
sudo dnf install wkhtmltopdf
```
##### 1-) General Section
```yaml
general:
    logoImagePath: "images/logo.png" # PDF içerisinde yer alan logo.
    reportsPath: "/opt/reports" # PDF Raporun oluşturulacağı klasör yolu
```

##### 1-) Logging Section
```yaml
logging:
    fileName: "ulak.log" # Uygulama log dosyası
    level: "INFO" # Default log seviyesi
```

##### 1-) Zabbix Section
```yaml
zabbix:
    url:
        value: "https://zabbix.local"
#        fromEnv:
#            value: "ZABBIX_URL"
    username:
        value: "api_user"
#        fromEnv:
#            value: "ZABBIX_USERNAME"
    password:
        value: "123456"
#        fromEnv:
#            value: "ZABBIX_PASSWORD"
```

##### 1-) Report Section
```yaml
report:
    period: "daily"  # or weekly, monthly
    # Eğer hostGroups seçeneği tanımlanmışsa, hosts seçeneği dikkate alınmaz
    hostGroups:
        - "Linux servers"
#     hosts:
#         - ""
    # Rapor için toplanacak item isimleri (Büyük/küçük harf duyarlı)
    resourceItems:
        - "Available memory"
        - "Available memory in %"
        - "Free swap space"
        - "Processor load (1 min average per core)"
        - "System uptime"
```

##### 1-) Sender Section
```yaml
sender:
    mailAddress:
        value: "root@local"
#        fromEnv:
#            value: "ZABBIX_REPORT_SENDER"
    fromAddress:
        value: "Zabbix Reports"
#        fromEnv:
#            value: "ZABBIX_REPORT_SENDER_FROM"
    password:
        value: "123456"
#        fromEnv:
#            value: "ZABBIX_REPORT_SENDER_PASSWORD"
    mailServer:
        value: "mail.local"
#        fromEnv:
#            value: "ZABBIX_REPORT_MAIL_SERVER"
    mailServerPort:
        value: "587"
```

##### 1-) Recipients Section
```yaml
recipients:
    to:
        - "foo@local"
        - "bar@local"
    cc:
        - "foo@local"
    bcc:
        - "foo@local"
```

## Ekran Görüntüleri
##### Resource Usage Report
![resource_tracking](screenshots/1.png)

##### Checklist Report
![checklist](screenshots/3.png)

##### Events Report
![event](screenshots/2.png)